# bin bash
# Script para configurar Virtualbox después de actualizar a Fedora 36
mkdir vb_config_uefi
cd vb_config_uefi
touch segundo_script.sh
sudo -i
sudo dnf -y upgrade
sudo dnf -y install @development-tools
sudo dnf -y install kernel-headers kernel-devel dkms elfutils-libelf-devel qt5-qtx11extras
dnf install -y kernel-devel-$(uname -r) kernel-headers
rpm --import  https://www.virtualbox.org/download/oracle_vbox.asc
dnf config-manager --add-repo https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
sudo dnf update && sudo dnf upgrade
dnf install -y VirtualBox-6.1
usermod -aG vboxusers $USER
sudo -i
newgrp vboxusers
dnf install mokutil
dnf install -y openssl 
sudo -i
mkdir /root/signed-modules
cd /root/signed-modules
openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=VirtualBox/"
chmod 600 MOK.priv
sudo mokutil --import MOK.der
sudo -i
cd /root/signed-modules
#_______________________________________________________________
(
cat << 'EOF' 
#!/bin/bash

for modfile in $(dirname $(modinfo -n vboxdrv))/*.ko; do
  echo "Signing $modfile"
  /usr/src/kernels/$(uname -r)/scripts/sign-file sha256 \
                                /root/signed-modules/MOK.priv \
                                /root/signed-modules/MOK.der "$modfile"
done
EOF
) >> sign-virtual-box
exit
cd /home/$USER/Descargas/vb_config_uefi

#______________________________________________________________
(
cat << 'EOF' 
sudo -i
cd /root/signed-modules
find /usr/src -name signfile
chmod 700 sign-virtual-box
./sign-virtual-box 
modprobe vboxdrv
EOF
) >> segundo_script.sh
#_______________________________________________________________
if [ -f "imprimir.sh" ]
then
  chmod 755 segundo_script.sh
  # Make the generated file executable.
else
  echo "Problem in creating file: \"$OUTFILE\""
fi
sudo reboot



