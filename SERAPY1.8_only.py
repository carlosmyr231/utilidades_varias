from datetime import date

from stat import S_IFDIR
import string
from blabel import LabelWriter
import os
from html2text import re
import csv
from datetime import datetime
import shutil
from PyPDF2 import PdfFileMerger

os.system('clear')
print("SERAPY Versión 1.8 Copy Left 2022 by MyR")
os.system("rm *.pdf")
print ("Leyendo Formato ")
label_writer=LabelWriter("item_template.html",default_stylesheets=("style.css",))
print ("Leyendo Archivo de Lista")
#lector variable que venga del excel
file = open("Lista.csv")
csvreader = csv.reader(file)
#print(csvreader)
header = next(csvreader)
#print(header)
rows = []
for row in csvreader:
    rows.append(row)
    print("***************** Datos a procesar *************")
    print(rows)
    file.close()
    print("***************** Asignando Variables *************")
    #print(type(rows))
    #print(row)
    #print(*rows[0:])
    rango = (len(rows))
    print("***************** Constructor *************")
    contador = 0
    resultado = 0
for contador in range (rango):
    Etiquetador = (rows[contador])
    #print(Etiquetador)
    #print(contador)
    #print(resultado)
    Etiquetador = (str(*Etiquetador))
    resultado = resultado + contador
    records = [dict(sample_id = Etiquetador , sample_name = Etiquetador)]
    label_writer.write_labels(records, target = (datetime.today().strftime('%Y-%m-%d %H:%M'"_")) + (str(contador)) +'.pdf',base_url='.')
    
os.mkdir((datetime.today().strftime('%Y-%m-%d %H:%M'"_SERAPY")))
dir_tiempo = (datetime.today().strftime('%Y-%m-%d %H:%M'"_SERAPY"))


print("***************** Fusionador *************")

pdfs = [archivo for archivo in os.listdir('./') if archivo.endswith(".pdf")]
nombre_archivo_salida = (datetime.today().strftime('%Y-%m-%d %H:%M'"_JOIN")) + (str(contador)) +'.pdf'
fusionador = PdfFileMerger()

for pdf in pdfs:
    fusionador.append(open(pdf, 'rb'))

with open(nombre_archivo_salida, 'wb') as salida:
    fusionador.write(salida)

shutil.move(str(nombre_archivo_salida) , (str(dir_tiempo)))
os.system("rm *.pdf")

print("Proceso Terminado")
print("SERAPY Versión 1.8 Copy Left 2022 by MyR")


